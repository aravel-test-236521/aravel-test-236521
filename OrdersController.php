<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData(Request $request)
    { 
        // your logic goes here.
        //return [];

        /* */
        $orderid = $request->id;
         $query = DB::table('orders AS A')
            ->select(DB::raw("A.orderNumber As order_id,A.orderDate,a.status,c.productName As product,c.productLine,B.priceEach as unit_price,B.quantityOrdered as quantity,(B.priceEach * B.quantityOrdered) AS line_total
                "))
            ->join(DB::raw('orderdetails AS B'), DB::raw('A.orderNumber'), '=', DB::raw('B.orderNumber'))
            ->join(DB::raw('PRODUCTS AS C'), DB::raw('B.productCode '), '=', DB::raw('C.productCode'))
            ->where('A.orderNumber', $orderid)
            ->get();

         $query1 = DB::table('orderdetails')
            ->select()
            ->where('orderNumber', $orderid)
            ->get();

         $query2 = DB::table('orders AS A')
            ->select(DB::raw("contactfirstName AS first_name ,contactLastName AS last_name,phone,country as country_code
                "))
             ->join(DB::raw('customers D'), DB::raw('A.customerNumber'), '=', DB::raw('D.customerNumber'))
            ->where('A.orderNumber', $orderid)
            ->get();

            $query4 = DB::table('orders AS A')
            ->select(DB::raw("sum((B.priceEach * B.quantityOrdered)) AS bill_amount"))
            ->join(DB::raw('orderdetails AS B'), DB::raw('A.orderNumber'), '=', DB::raw('B.orderNumber'))
            ->where('A.orderNumber', $orderid)
            ->get();
   

            $data = array();
            $bill_amount = 0;
            foreach ($query as $key) {
                $bill_amount += $key->line_total;
                $key->order_details=$query1;
                $key->bill_amount = $query4;
                $key->customer=$query2;
                $data=$key;
            }
            
                

            //$data = $query2;


            return response()->json($data);
            
    }
}
